// Copyright 2017-2019 Autoware Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <skoda_interface/skoda_interface.hpp>

#include <algorithm>
#include <limits>
#include <memory>
#include <utility>

SkodaInterface::SkodaInterface()
: Node("skoda_interface"),
  vehicle_info_(vehicle_info_util::VehicleInfoUtil(*this).getVehicleInfo())
{
  /* setup parameters */
  base_frame_id_ = declare_parameter("base_frame_id", "base_link");
  command_timeout_ms_ = declare_parameter("command_timeout_ms", 1000);
  loop_rate_ = declare_parameter("loop_rate", 30.0);

  /* parameters for vehicle specifications */
  tire_radius_ = vehicle_info_.wheel_radius_m;
  wheel_base_ = vehicle_info_.wheel_base_m;

  steering_offset_ = declare_parameter("steering_offset", 0.0);
  enable_steering_rate_control_ = declare_parameter("enable_steering_rate_control", false);

  /* parameters for emergency stop */
  emergency_brake_ = declare_parameter("emergency_brake", 0.7);

  /* vehicle parameters */
  vgr_coef_a_ = declare_parameter("vgr_coef_a", 15.713);
  vgr_coef_b_ = declare_parameter("vgr_coef_b", 0.053);
  vgr_coef_c_ = declare_parameter("vgr_coef_c", 0.042);
  accel_pedal_offset_ = declare_parameter("accel_pedal_offset", 0.0);
  brake_pedal_offset_ = declare_parameter("brake_pedal_offset", 0.0);
  speed_scale_factor_ = declare_parameter("speed_scale_factor", 1.0);

  /* parameters for limitter */
  max_throttle_ = declare_parameter("max_throttle", 0.2);
  max_brake_ = declare_parameter("max_brake", 0.8);
  max_steering_wheel_ = declare_parameter("max_steering_wheel", 2.7 * M_PI);
  max_steering_wheel_rate_ = declare_parameter("max_steering_wheel_rate", 6.6);
  min_steering_wheel_rate_ = declare_parameter("min_steering_wheel_rate", 0.5);
  steering_wheel_rate_low_vel_ = declare_parameter("steering_wheel_rate_low_vel", 5.0);
  steering_wheel_rate_stopped_ = declare_parameter("steering_wheel_rate_stopped", 5.0);
  low_vel_thresh_ = declare_parameter("low_vel_thresh", 1.389);  // 5.0kmh

  /* parameters for turn signal recovery */
  hazard_thresh_time_ = declare_parameter("hazard_thresh_time", 0.20);  // s
  /* initialize */

  prev_steer_cmd_.header.stamp = this->now();
  // prev_steer_cmd_.command = 0.0;
  prev_steer_cmd_.scmd = 0.0;

  /* subscribers */
  using std::placeholders::_1;
  using std::placeholders::_2;

  // From autoware
  control_cmd_sub_ = create_subscription<autoware_auto_control_msgs::msg::AckermannControlCommand>(
    "/control/command/control_cmd", 1, std::bind(&SkodaInterface::callbackControlCmd, this, _1));
  gear_cmd_sub_ = create_subscription<autoware_auto_vehicle_msgs::msg::GearCommand>(
    "/control/command/gear_cmd", 1, std::bind(&SkodaInterface::callbackGearCmd, this, _1));
  turn_indicators_cmd_sub_ =
    create_subscription<autoware_auto_vehicle_msgs::msg::TurnIndicatorsCommand>(
      "/control/command/turn_indicators_cmd", rclcpp::QoS{1},
      std::bind(&SkodaInterface::callbackTurnIndicatorsCommand, this, _1));
  hazard_lights_cmd_sub_ =
    create_subscription<autoware_auto_vehicle_msgs::msg::HazardLightsCommand>(
      "/control/command/hazard_lights_cmd", rclcpp::QoS{1},
      std::bind(&SkodaInterface::callbackHazardLightsCommand, this, _1));

  actuation_cmd_sub_ = create_subscription<ActuationCommandStamped>(
    "/control/command/actuation_cmd", 1,
    std::bind(&SkodaInterface::callbackActuationCmd, this, _1));
  emergency_sub_ = create_subscription<tier4_vehicle_msgs::msg::VehicleEmergencyStamped>(
    "/control/command/emergency_cmd", 1,
    std::bind(&SkodaInterface::callbackEmergencyCmd, this, _1));
  control_mode_server_ = create_service<tier4_vehicle_msgs::srv::ControlModeRequest>(
    "input/control_mode_request", std::bind(&SkodaInterface::onControlModeRequest, this, _1, _2));

  #if 0
  // From pacmod

  steer_wheel_rpt_sub_ =
    std::make_unique<message_filters::Subscriber<pacmod3_msgs::msg::SystemRptFloat>>(
      this, "/pacmod/steering_rpt");
  wheel_speed_rpt_sub_ =
    std::make_unique<message_filters::Subscriber<pacmod3_msgs::msg::WheelSpeedRpt>>(
      this, "/pacmod/wheel_speed_rpt");
  accel_rpt_sub_ = std::make_unique<message_filters::Subscriber<pacmod3_msgs::msg::SystemRptFloat>>(
    this, "/pacmod/accel_rpt");
  brake_rpt_sub_ = std::make_unique<message_filters::Subscriber<pacmod3_msgs::msg::SystemRptFloat>>(
    this, "/pacmod/brake_rpt");
  shift_rpt_sub_ = std::make_unique<message_filters::Subscriber<pacmod3_msgs::msg::SystemRptInt>>(
    this, "/pacmod/shift_rpt");
  turn_rpt_sub_ = std::make_unique<message_filters::Subscriber<pacmod3_msgs::msg::SystemRptInt>>(
    this, "/pacmod/turn_rpt");
  global_rpt_sub_ = std::make_unique<message_filters::Subscriber<pacmod3_msgs::msg::GlobalRpt>>(
    this, "/pacmod/global_rpt");
  rear_door_rpt_sub_ =
    std::make_unique<message_filters::Subscriber<pacmod3_msgs::msg::SystemRptInt>>(
      this, "/pacmod/rear_pass_door_rpt");

  pacmod_feedbacks_sync_ =
    std::make_unique<message_filters::Synchronizer<PacmodFeedbacksSyncPolicy>>(
      PacmodFeedbacksSyncPolicy(10), *steer_wheel_rpt_sub_, *wheel_speed_rpt_sub_, *accel_rpt_sub_,
      *brake_rpt_sub_, *shift_rpt_sub_, *turn_rpt_sub_, *global_rpt_sub_, *rear_door_rpt_sub_);

  pacmod_feedbacks_sync_->registerCallback(std::bind(
    &SkodaInterface::callbackPacmodRpt, this, std::placeholders::_1, std::placeholders::_2,
    std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6,
    std::placeholders::_7, std::placeholders::_8));
  #endif 


  // From Skoda
  steering_report_sub_ = std::make_unique<message_filters::Subscriber<remote_by_wire_driver_msgs::msg::SteeringReport>>(
      this, "/steering_report");  // 20ms
  throttle_report_sub_ = std::make_unique<message_filters::Subscriber<remote_by_wire_driver_msgs::msg::ThrottleReport>>(
      this, "/throttle_report");  // 20ms
  brake_report_sub_ = std::make_unique<message_filters::Subscriber<remote_by_wire_driver_msgs::msg::BrakeReport>>(
      this, "/brake_report");     // 20ms
  // accel_report_sub_ = std::make_unique<message_filters::Subscriber<remote_by_wire_driver_msgs::msg::AccelReport>>(
  //     this, "/accel_report");     // 10ms
  fuel_level_report_sub_ = std::make_unique<message_filters::Subscriber<remote_by_wire_driver_msgs::msg::FuelLevelReport>>(
      this, "/fuel_level_report"); // 100ms
  misc_report_sub_ = std::make_unique<message_filters::Subscriber<remote_by_wire_driver_msgs::msg::MiscReport>>(
      this, "/misc_report");
  wheel_spd_pulse_report_sub_ = std::make_unique<message_filters::Subscriber<remote_by_wire_driver_msgs::msg::WheelSPDPulseReport>>(
      this, "/wheel_spd_pulse_report");
  wheel_speed_report_sub_ = std::make_unique<message_filters::Subscriber<remote_by_wire_driver_msgs::msg::WheelSpeedReport>>(
      this, "/wheel_speed_report");
  twist_report_sub_ = std::make_unique<message_filters::Subscriber< geometry_msgs::msg::TwistStamped>>(
      this, "/twist_report");

  // 线控状态反馈
  vehicle_system_report_sub_ = create_subscription<remote_by_wire_driver_msgs::msg::SystemReport>(
            "/system_report", 1, std::bind(&SkodaInterface::callbackSystemReport, this, _1));

  gear_report_sub_ = create_subscription<remote_by_wire_driver_msgs::msg::GearReport>(
            "/gear_report", 1, std::bind(&SkodaInterface::callbackGearReport, this, _1));

  accel_report_sub_ = create_subscription<remote_by_wire_driver_msgs::msg::AccelReport>(
            "/accel_report", 1, std::bind(&SkodaInterface::callbackAccelReport, this, _1));


  skoda_feedbacks_sync_ =
    std::make_unique<message_filters::Synchronizer<SkodaFeedbacksSyncPolicy>>(
        SkodaFeedbacksSyncPolicy( 10), 
        *steering_report_sub_,
        *throttle_report_sub_, 
        *brake_report_sub_ 
        );

  skoda_feedbacks_sync_->registerCallback(std::bind(
          &SkodaInterface::callbackSkodaRpt, this, 
          std::placeholders::_1, 
          std::placeholders::_2,
          std::placeholders::_3
        )
      );

  /* publisher */

  // // To pacmod
  // accel_cmd_pub_ =
  //   create_publisher<pacmod3_msgs::msg::SystemCmdFloat>("/pacmod/accel_cmd", rclcpp::QoS{1});
  // brake_cmd_pub_ =
  //   create_publisher<pacmod3_msgs::msg::SystemCmdFloat>("/pacmod/brake_cmd", rclcpp::QoS{1});
  // steer_cmd_pub_ =
  //   create_publisher<pacmod3_msgs::msg::SteeringCmd>("/pacmod/steering_cmd", rclcpp::QoS{1});
  // shift_cmd_pub_ =
  //   create_publisher<pacmod3_msgs::msg::SystemCmdInt>("/pacmod/shift_cmd", rclcpp::QoS{1});
  // turn_cmd_pub_ =
  //   create_publisher<pacmod3_msgs::msg::SystemCmdInt>("/pacmod/turn_cmd", rclcpp::QoS{1});
  // door_cmd_pub_ =
  //   create_publisher<pacmod3_msgs::msg::SystemCmdInt>("/pacmod/rear_pass_door_cmd", rclcpp::QoS{1});
  // raw_steer_cmd_pub_ = create_publisher<pacmod3_msgs::msg::SteeringCmd>(
  //   "/pacmod/raw_steer_cmd", rclcpp::QoS{1});  // only for debug

  // To skoda
  turn_signal_command_pub_= create_publisher<remote_by_wire_driver_msgs::msg::TurnSignalCommand>( "/turn_signal_command", rclcpp::QoS{1}); 
  gear_command_pub_     = create_publisher<remote_by_wire_driver_msgs::msg::GearCommand>( "/gear_command", 1 );
  steering_command_pub_ = create_publisher<remote_by_wire_driver_msgs::msg::SteeringCommand>( "/steering_command", 1);
  throttle_command_pub_ = create_publisher<remote_by_wire_driver_msgs::msg::ThrottleCommand>( "/throttle_command", 1);
  brake_command_pub_    = create_publisher<remote_by_wire_driver_msgs::msg::BrakeCommand>( "/brake_command", 1);


  // To Autoware
  control_mode_pub_ = create_publisher<autoware_auto_vehicle_msgs::msg::ControlModeReport>(
    "/vehicle/status/control_mode", rclcpp::QoS{1});
  vehicle_twist_pub_ = create_publisher<autoware_auto_vehicle_msgs::msg::VelocityReport>(
    "/vehicle/status/velocity_status", rclcpp::QoS{1});
  steering_status_pub_ = create_publisher<autoware_auto_vehicle_msgs::msg::SteeringReport>(
    "/vehicle/status/steering_status", rclcpp::QoS{1});
  gear_status_pub_ = create_publisher<autoware_auto_vehicle_msgs::msg::GearReport>(
    "/vehicle/status/gear_status", rclcpp::QoS{1});
  turn_indicators_status_pub_ =
    create_publisher<autoware_auto_vehicle_msgs::msg::TurnIndicatorsReport>(
      "/vehicle/status/turn_indicators_status", rclcpp::QoS{1});
  hazard_lights_status_pub_ = create_publisher<autoware_auto_vehicle_msgs::msg::HazardLightsReport>(
    "/vehicle/status/hazard_lights_status", rclcpp::QoS{1});
  actuation_status_pub_ =
    create_publisher<ActuationStatusStamped>("/vehicle/status/actuation_status", 1);
  steering_wheel_status_pub_ =
    create_publisher<SteeringWheelStatusStamped>("/vehicle/status/steering_wheel_status", 1);
  door_status_pub_ =
    create_publisher<tier4_api_msgs::msg::DoorStatus>("/vehicle/status/door_status", 1);

  /* service */
  //  From autoware
  tier4_api_utils::ServiceProxyNodeInterface proxy(this);
  srv_ = proxy.create_service<tier4_external_api_msgs::srv::SetDoor>(
    "/api/vehicle/set/door",
    std::bind(&SkodaInterface::setDoor, this, std::placeholders::_1, std::placeholders::_2));

  // Timer
  const auto period_ns = rclcpp::Rate(loop_rate_).period();
  timer_ = rclcpp::create_timer(
    this, get_clock(), period_ns, std::bind(&SkodaInterface::publishCommands, this));
}

void SkodaInterface::callbackActuationCmd(const ActuationCommandStamped::ConstSharedPtr msg)
{
  actuation_command_received_time_ = this->now();
  actuation_cmd_ptr_ = msg;
}

void SkodaInterface::callbackEmergencyCmd(
  const tier4_vehicle_msgs::msg::VehicleEmergencyStamped::ConstSharedPtr msg)
{
  is_emergency_ = msg->emergency;
}

void SkodaInterface::callbackControlCmd(
  const autoware_auto_control_msgs::msg::AckermannControlCommand::ConstSharedPtr msg)
{
  control_command_received_time_ = this->now();
  control_cmd_ptr_ = msg;
}

void SkodaInterface::callbackGearCmd(
  const autoware_auto_vehicle_msgs::msg::GearCommand::ConstSharedPtr msg)
{
  gear_cmd_ptr_ = msg;
}

void SkodaInterface::callbackTurnIndicatorsCommand(
  const autoware_auto_vehicle_msgs::msg::TurnIndicatorsCommand::ConstSharedPtr msg)
{
  turn_indicators_cmd_ptr_ = msg;
}

void SkodaInterface::callbackHazardLightsCommand(
  const autoware_auto_vehicle_msgs::msg::HazardLightsCommand::ConstSharedPtr msg)
{
  hazard_lights_cmd_ptr_ = msg;
}

void SkodaInterface::onControlModeRequest(
  const tier4_vehicle_msgs::srv::ControlModeRequest::Request::SharedPtr request,
  const tier4_vehicle_msgs::srv::ControlModeRequest::Response::SharedPtr response)
{
  if (request->mode.data == tier4_vehicle_msgs::msg::ControlMode::AUTO) {
    engage_cmd_ = true;
    is_clear_override_needed_ = true;
    response->success = true;
    return;
  }

  if (request->mode.data == tier4_vehicle_msgs::msg::ControlMode::MANUAL) {
    engage_cmd_ = false;
    is_clear_override_needed_ = true;
    response->success = true;
    return;
  }

  RCLCPP_ERROR(get_logger(), "unsupported control_mode!!");
  response->success = false;
  return;
}


void SkodaInterface::callbackSystemReport( remote_by_wire_driver_msgs::msg::SystemReport::ConstSharedPtr msg )
{
  is_skoda_rpt_received_ = true;
  system_report_rpt_ptr_ = msg;
}

void SkodaInterface::callbackGearReport( remote_by_wire_driver_msgs::msg::GearReport::ConstSharedPtr msg )
{
  gear_report_rpt_ptr_ = msg;
}

void SkodaInterface::callbackAccelReport( remote_by_wire_driver_msgs::msg::AccelReport::ConstSharedPtr msg )
{
  accel_report_rpt_ptr_ = msg;
}


void SkodaInterface::callbackSkodaRpt(
  remote_by_wire_driver_msgs::msg::SteeringReport::ConstSharedPtr steering_report_rpt_ptr,
  remote_by_wire_driver_msgs::msg::ThrottleReport::ConstSharedPtr throttle_report_rpt_ptr,
  remote_by_wire_driver_msgs::msg::BrakeReport::ConstSharedPtr brake_report_rpt_ptr )
{
  is_skoda_rpt_received_ = true;
  steering_report_rpt_ptr_= steering_report_rpt_ptr;
  throttle_report_rpt_ptr_= throttle_report_rpt_ptr;
  brake_report_rpt_ptr_   = brake_report_rpt_ptr;

  if (accel_report_rpt_ptr_ == nullptr  || !accel_report_rpt_ptr_ ){
    RCLCPP_ERROR( get_logger(),"accel_report_rpt_ptr_ == null");
    return;
  } 

  // if (system_report_rpt_ptr_ == nullptr){
  //   RCLCPP_ERROR( get_logger(),"system_report_rpt_ptr_ == null");
  //   return;
  // }
  // is_skoda_enabled_ = system_report_rpt_ptr_ -> wire_control_flag ;
  is_skoda_enabled_ = true; //debug

  // RCLCPP_INFO_THROTTLE
  RCLCPP_DEBUG( get_logger(), "enabled: is_skoda_enabled_ %d,  steering %d, throttle %d, " "brake %d  ",
      is_skoda_enabled_, 
      steering_report_rpt_ptr_->en, 
      throttle_report_rpt_ptr_->en, 
      brake_report_rpt_ptr_->en
    );

  // const double current_velocity = calculateVehicleVelocity( *wheel_speed_rpt_ptr_, *gear_cmd_rpt_ptr_);  // current vehicle speed > 0 [m/s]
  const double current_velocity = accel_report_rpt_ptr_ -> v_speed / 3.6 ;

  // const double current_steer_wheel = steer_wheel_rpt_ptr_->output;  // current vehicle steering wheel angle [rad]
  const double current_steer_wheel = steering_report_rpt_ptr->angle * -1 * (3.1415926535/180); // (rad)

  const double adaptive_gear_ratio = calculateVariableGearRatio(current_velocity, current_steer_wheel);
  const double current_steer = current_steer_wheel / adaptive_gear_ratio + steering_offset_;

  #if 0
  // current_steer  =  deg2rad(steering_report_rpt_ptr->angle / 13.2); // rad
  // skoda = 13.2
  // Lavida = 12.4
  #endif

  std_msgs::msg::Header header;
  header.frame_id = base_frame_id_;
  header.stamp = get_clock()->now();

  /* publish steering wheel status */
  {
    SteeringWheelStatusStamped steering_wheel_status_msg;
    steering_wheel_status_msg.stamp = header.stamp;
    steering_wheel_status_msg.data = current_steer_wheel;
    steering_wheel_status_pub_->publish(steering_wheel_status_msg);
  }
  /* publish vehicle status control_mode */
  {
    autoware_auto_vehicle_msgs::msg::ControlModeReport control_mode_msg;
    control_mode_msg.stamp = header.stamp;

    // if (global_rpt->enabled && is_pacmod_enabled_) {
    if (is_skoda_enabled_) {
      control_mode_msg.mode = autoware_auto_vehicle_msgs::msg::ControlModeReport::AUTONOMOUS;
    } else {
      control_mode_msg.mode = autoware_auto_vehicle_msgs::msg::ControlModeReport::MANUAL;
    }

    control_mode_pub_->publish(control_mode_msg);
  }

  #if 1
  /* publish vehicle status twist */
  {
    autoware_auto_vehicle_msgs::msg::VelocityReport twist;
    twist.header = header;
    twist.longitudinal_velocity = current_velocity;                                 // [m/s]
    twist.heading_rate = current_velocity * std::tan(current_steer) / wheel_base_;  // [rad/s]
    vehicle_twist_pub_->publish(twist);
  }
  #endif

  /* publish current shift */
  {
    autoware_auto_vehicle_msgs::msg::GearReport gear_report_msg;
    gear_report_msg.stamp = header.stamp;
    const auto opt_gear_report = toAutowareShiftReport(*gear_report_rpt_ptr_);
    if (opt_gear_report) {
      gear_report_msg.report = *opt_gear_report;
      gear_status_pub_->publish(gear_report_msg);
    }
  }

  /* publish current status */
  {
    autoware_auto_vehicle_msgs::msg::SteeringReport steer_msg;
    steer_msg.stamp = header.stamp;
    steer_msg.steering_tire_angle = current_steer;
    steering_status_pub_->publish(steer_msg);
  }

  /* publish control status */
  {
    ActuationStatusStamped actuation_status;
    actuation_status.header = header;
    // actuation_status.status.accel_status =  accel_rpt_ptr_->output;
    // actuation_status.status.brake_status = brake_rpt_ptr_->output;
    actuation_status.status.accel_status = throttle_report_rpt_ptr_->pi/100; // [0-1]
    actuation_status.status.brake_status = brake_report_rpt_ptr_->pc /100;   // [0-1]
    actuation_status.status.steer_status = current_steer;
    actuation_status_pub_->publish(actuation_status);
  }

#if 0
  /* publish current turn signal */
  {
    autoware_auto_vehicle_msgs::msg::TurnIndicatorsReport turn_msg;
    turn_msg.stamp = header.stamp;
    turn_msg.report = toAutowareTurnIndicatorsReport(*turn_rpt);
    turn_indicators_status_pub_->publish(turn_msg);

    autoware_auto_vehicle_msgs::msg::HazardLightsReport hazard_msg;
    hazard_msg.stamp = header.stamp;
    hazard_msg.report = toAutowareHazardLightsReport(*turn_rpt);
    hazard_lights_status_pub_->publish(hazard_msg);
  }

  /* publish current door status */
  {
    door_status_pub_->publish(toAutowareDoorStatusMsg(*rear_door_rpt));
  }

#endif

}

#if 0
void SkodaInterface::callbackPacmodRpt(
  const pacmod3_msgs::msg::SystemRptFloat::ConstSharedPtr steer_wheel_rpt,
  const pacmod3_msgs::msg::WheelSpeedRpt::ConstSharedPtr wheel_speed_rpt,
  const pacmod3_msgs::msg::SystemRptFloat::ConstSharedPtr accel_rpt,
  const pacmod3_msgs::msg::SystemRptFloat::ConstSharedPtr brake_rpt,
  const pacmod3_msgs::msg::SystemRptInt::ConstSharedPtr shift_rpt,
  const pacmod3_msgs::msg::SystemRptInt::ConstSharedPtr turn_rpt,
  const pacmod3_msgs::msg::GlobalRpt::ConstSharedPtr global_rpt,
  const pacmod3_msgs::msg::SystemRptInt::ConstSharedPtr rear_door_rpt)
{
  is_pacmod_rpt_received_ = true;
  steer_wheel_rpt_ptr_ = steer_wheel_rpt;
  wheel_speed_rpt_ptr_ = wheel_speed_rpt;
  accel_rpt_ptr_ = accel_rpt;
  brake_rpt_ptr_ = brake_rpt;
  gear_cmd_rpt_ptr_ = shift_rpt;
  global_rpt_ptr_ = global_rpt;
  turn_rpt_ptr_ = turn_rpt;

  is_pacmod_enabled_ =
    steer_wheel_rpt_ptr_->enabled && accel_rpt_ptr_->enabled && brake_rpt_ptr_->enabled;
  RCLCPP_DEBUG(
    get_logger(),
    "enabled: is_pacmod_enabled_ %d, steer %d, accel %d, brake %d, shift %d, "
    "global %d",
    is_pacmod_enabled_, steer_wheel_rpt_ptr_->enabled, accel_rpt_ptr_->enabled,
    brake_rpt_ptr_->enabled, gear_cmd_rpt_ptr_->enabled, global_rpt_ptr_->enabled);

  const double current_velocity = calculateVehicleVelocity(
    *wheel_speed_rpt_ptr_, *gear_cmd_rpt_ptr_);  // current vehicle speed > 0 [m/s]
  const double current_steer_wheel =
    steer_wheel_rpt_ptr_->output;  // current vehicle steering wheel angle [rad]
  const double adaptive_gear_ratio =
    calculateVariableGearRatio(current_velocity, current_steer_wheel);
  const double current_steer = current_steer_wheel / adaptive_gear_ratio + steering_offset_;

  std_msgs::msg::Header header;
  header.frame_id = base_frame_id_;
  header.stamp = get_clock()->now();

  /* publish steering wheel status */
  {
    SteeringWheelStatusStamped steering_wheel_status_msg;
    steering_wheel_status_msg.stamp = header.stamp;
    steering_wheel_status_msg.data = current_steer_wheel;
    steering_wheel_status_pub_->publish(steering_wheel_status_msg);
  }

  /* publish vehicle status control_mode */
  {
    autoware_auto_vehicle_msgs::msg::ControlModeReport control_mode_msg;
    control_mode_msg.stamp = header.stamp;

    if (global_rpt->enabled && is_pacmod_enabled_) {
      control_mode_msg.mode = autoware_auto_vehicle_msgs::msg::ControlModeReport::AUTONOMOUS;
    } else {
      control_mode_msg.mode = autoware_auto_vehicle_msgs::msg::ControlModeReport::MANUAL;
    }

    control_mode_pub_->publish(control_mode_msg);
  }

  /* publish vehicle status twist */
  {
    autoware_auto_vehicle_msgs::msg::VelocityReport twist;
    twist.header = header;
    twist.longitudinal_velocity = current_velocity;                                 // [m/s]
    twist.heading_rate = current_velocity * std::tan(current_steer) / wheel_base_;  // [rad/s]
    vehicle_twist_pub_->publish(twist);
  }

  /* publish current shift */
  {
    autoware_auto_vehicle_msgs::msg::GearReport gear_report_msg;
    gear_report_msg.stamp = header.stamp;
    const auto opt_gear_report = toAutowareShiftReport(*gear_cmd_rpt_ptr_);
    if (opt_gear_report) {
      gear_report_msg.report = *opt_gear_report;
      gear_status_pub_->publish(gear_report_msg);
    }
  }

  /* publish current status */
  {
    autoware_auto_vehicle_msgs::msg::SteeringReport steer_msg;
    steer_msg.stamp = header.stamp;
    steer_msg.steering_tire_angle = current_steer;
    steering_status_pub_->publish(steer_msg);
  }

  /* publish control status */
  {
    ActuationStatusStamped actuation_status;
    actuation_status.header = header;
    actuation_status.status.accel_status = accel_rpt_ptr_->output;
    actuation_status.status.brake_status = brake_rpt_ptr_->output;
    actuation_status.status.steer_status = current_steer;
    actuation_status_pub_->publish(actuation_status);
  }

  /* publish current turn signal */
  {
    autoware_auto_vehicle_msgs::msg::TurnIndicatorsReport turn_msg;
    turn_msg.stamp = header.stamp;
    turn_msg.report = toAutowareTurnIndicatorsReport(*turn_rpt);
    turn_indicators_status_pub_->publish(turn_msg);

    autoware_auto_vehicle_msgs::msg::HazardLightsReport hazard_msg;
    hazard_msg.stamp = header.stamp;
    hazard_msg.report = toAutowareHazardLightsReport(*turn_rpt);
    hazard_lights_status_pub_->publish(hazard_msg);
  }

  /* publish current door status */
  {
    door_status_pub_->publish(toAutowareDoorStatusMsg(*rear_door_rpt));
  }
}
#endif

void SkodaInterface::publishCommands()
{
  /* guard */
  if (!actuation_cmd_ptr_ || !control_cmd_ptr_ || !is_skoda_rpt_received_ || !gear_cmd_ptr_) {
    RCLCPP_INFO_THROTTLE(
      get_logger(), *get_clock(), std::chrono::milliseconds(1000).count(),
      "vehicle_cmd = %d, pacmod3_msgs = %d", actuation_cmd_ptr_ != nullptr,
      is_pacmod_rpt_received_);
    return;
  }

  const rclcpp::Time current_time = get_clock()->now();

  double desired_throttle = actuation_cmd_ptr_->actuation.accel_cmd + accel_pedal_offset_;
  double desired_brake = actuation_cmd_ptr_->actuation.brake_cmd + brake_pedal_offset_;
  if (actuation_cmd_ptr_->actuation.brake_cmd <= std::numeric_limits<double>::epsilon()) {
    desired_brake = 0.0;
  }

  /* check emergency and timeout */
  const double control_cmd_delta_time_ms =
    (current_time - control_command_received_time_).seconds() * 1000.0;
  const double actuation_cmd_delta_time_ms =
    (current_time - actuation_command_received_time_).seconds() * 1000.0;
  bool timeouted = false;
  const int t_out = command_timeout_ms_;
  if (t_out >= 0 && (control_cmd_delta_time_ms > t_out || actuation_cmd_delta_time_ms > t_out)) {
    timeouted = true;
  }
  if (is_emergency_ || timeouted) {
    RCLCPP_ERROR(
      get_logger(), "Emergency Stopping, emergency = %d, timeouted = %d", is_emergency_, timeouted);
    desired_throttle = 0.0;
    desired_brake = emergency_brake_;
  }


  if (accel_report_rpt_ptr_ == nullptr || steering_report_rpt_ptr_ == nullptr || throttle_report_rpt_ptr_  ==nullptr || \
      brake_report_rpt_ptr_ == nullptr || gear_report_rpt_ptr_ == nullptr) {
      RCLCPP_ERROR(get_logger(), "report_rpt_ptr_ = nullptr");
      return;
  }


  // const double current_velocity = calculateVehicleVelocity(*wheel_speed_rpt_ptr_, *gear_cmd_rpt_ptr_);
  const double current_velocity = accel_report_rpt_ptr_->v_speed / 3.6;

  // const double current_steer_wheel = steer_wheel_rpt_ptr_->output;
  const double current_steer_wheel = deg2rad(steering_report_rpt_ptr_->angle * -1); //  (rad)

  /* calculate desired steering wheel */
  double adaptive_gear_ratio = calculateVariableGearRatio(current_velocity, current_steer_wheel);
  double desired_steer_wheel = (control_cmd_ptr_->lateral.steering_tire_angle - steering_offset_) * adaptive_gear_ratio;
  desired_steer_wheel = std::min(std::max(desired_steer_wheel, -max_steering_wheel_), max_steering_wheel_);

  /* check clear flag */
  bool clear_override = false;
  if (is_pacmod_enabled_ == true) {
    is_clear_override_needed_ = false;
  } else if (is_clear_override_needed_ == true) {
    clear_override = true;
  }

  /* make engage cmd false when a driver overrides vehicle control */
  // if (!prev_override_ && global_rpt_ptr_->override_active) {
  //   RCLCPP_WARN_THROTTLE(
  //     get_logger(), *get_clock(), std::chrono::milliseconds(1000).count(),
  //     "Pacmod is overridden, enable flag is back to false");
  //   engage_cmd_ = false;
  // }
  // prev_override_ = global_rpt_ptr_->override_active;

  if ( steering_report_rpt_ptr_->driver == true   
       || brake_report_rpt_ptr_ ->driver == true 
       ||  throttle_report_rpt_ptr_ -> driver == true)
  {

    RCLCPP_WARN_THROTTLE( get_logger(), *get_clock(), std::chrono::milliseconds(1000).count(),
      "skoda is overridden, enable flag is back to false");
       engage_cmd_ = false;
  }


  // /* make engage cmd false when vehicle report is timed out, e.g. E-stop is depressed */
  // const bool report_timed_out = ((current_time - global_rpt_ptr_->header.stamp).seconds() > 1.0);
  // if (report_timed_out) {
  //   RCLCPP_WARN_THROTTLE(
  //     get_logger(), *get_clock(), std::chrono::milliseconds(1000).count(),
  //     "Pacmod report is timed out, enable flag is back to false");
  //   engage_cmd_ = false;
  // }

  /* 
    make engage cmd false when vehicle fault is active 
  */
  if (brake_report_rpt_ptr_->flt1 || throttle_report_rpt_ptr_ ->flt1 || steering_report_rpt_ptr_ -> flt1 
      /*||fuel_level_report_rpt_ptr_ -> fuel < 20*/) {
      RCLCPP_WARN_THROTTLE( get_logger(), *get_clock(), std::chrono::milliseconds(1000).count(),
                            "Skoda fault is active, enable flag is back to false");
      engage_cmd_ = false;
  }

  RCLCPP_DEBUG(get_logger(), "is_skoda_enabled_ = %d, is_clear_override_needed_ = %d, clear_override = %d",
    is_skoda_enabled_, is_clear_override_needed_, clear_override);


  /* 
    check shift change 
  */
 #if 0
  const double brake_for_shift_trans = 0.7;
  uint16_t desired_shift = gear_cmd_rpt_ptr_->output;
  if (std::fabs(current_velocity) < 0.1) {  // velocity is low -> the shift can be changed
    if (toPacmodShiftCmd(*gear_cmd_ptr_) != gear_cmd_rpt_ptr_->output) {  // need shift
                                                                          // change.
      desired_throttle = 0.0;
      desired_brake = brake_for_shift_trans;  // set brake to change the shift
      desired_shift = toPacmodShiftCmd(*gear_cmd_ptr_);
      RCLCPP_DEBUG(
        get_logger(), "Doing shift change. current = %d, desired = %d. set brake_cmd to %f",
        gear_cmd_rpt_ptr_->output, toPacmodShiftCmd(*gear_cmd_ptr_), desired_brake);
    }
  }
  #endif

  const double brake_for_shift_trans = 0.7;

  if (gear_report_rpt_ptr_ != nullptr){
    uint16_t desired_shift =  gear_report_rpt_ptr_ ->cmd;
    if (std::fabs(current_velocity) < 0.1) {  // velocity is low -> the shift can be changed
      if (toSkodaShiftCmd(*gear_cmd_ptr_) != gear_report_rpt_ptr_->cmd) {  // need shift change.
        desired_throttle = 0.0;
        desired_brake = brake_for_shift_trans;  // set brake to change the shift
        desired_shift = toSkodaShiftCmd(*gear_cmd_ptr_);
        RCLCPP_DEBUG( get_logger(), "Doing shift change. current = %d, desired = %d. set brake_cmd to %f",
              gear_report_rpt_ptr_->cmd, toSkodaShiftCmd(*gear_cmd_ptr_), desired_brake);
      }
    }

    /* publish shift cmd */
    remote_by_wire_driver_msgs::msg::GearCommand gear_cmd;
    gear_cmd.header.frame_id = base_frame_id_;
    gear_cmd.header.stamp = current_time;
    gear_cmd.en = engage_cmd_;
    gear_cmd.clear = clear_override;
    gear_cmd.gcmd = desired_shift;
    gear_command_pub_->publish(gear_cmd);

  }


  // To Skoda

  /* publish accel cmd */
  {
    remote_by_wire_driver_msgs::msg::ThrottleCommand  throttle_command;
    throttle_command.header.frame_id = base_frame_id_;
    throttle_command.header.stamp = current_time;
    throttle_command.en = engage_cmd_ ;
    throttle_command.ignore = false ;
    throttle_command.clear = clear_override ;
    throttle_command.pcmd = std::max(0.0, std::min(desired_throttle, max_throttle_)) * 100; 
    throttle_command_pub_-> publish(throttle_command);
  }

  /* publish brake command */
  {
    remote_by_wire_driver_msgs::msg::BrakeCommand brake_command;
    brake_command.header.frame_id = base_frame_id_;
    brake_command.header.stamp = current_time;
    brake_command.en = engage_cmd_;
    brake_command.clear = clear_override;
    brake_command.pcmd = std::max(0.0, std::min(desired_brake, max_brake_)) * 100;
    brake_command_pub_->publish(brake_command);
  }



  /* publish steering cmd */
  // {
  //   pacmod3_msgs::msg::SteeringCmd steer_cmd;
  //   steer_cmd.header.frame_id = base_frame_id_;
  //   steer_cmd.header.stamp = current_time;
  //   steer_cmd.enable = engage_cmd_;
  //   steer_cmd.ignore_overrides = false;
  //   steer_cmd.clear_override = clear_override;
  //   steer_cmd.rotation_rate = calcSteerWheelRateCmd(adaptive_gear_ratio);
  //   steer_cmd.command = steerWheelRateLimiter(
  //     desired_steer_wheel, prev_steer_cmd_.command, current_time, prev_steer_cmd_.header.stamp,
  //     steer_cmd.rotation_rate, current_steer_wheel, engage_cmd_);
  //   steer_cmd_pub_->publish(steer_cmd);
  //   prev_steer_cmd_ = steer_cmd;
  // }

  /* publish steering cmd */
  {
    remote_by_wire_driver_msgs::msg::SteeringCommand steer_cmd;
    steer_cmd.header.frame_id = base_frame_id_;
    steer_cmd.header.stamp = current_time;
    steer_cmd.en = engage_cmd_;
    steer_cmd.clear = false ; // clear_override;

    // steer_cmd.svl = calcSteerWheelRateCmd(adaptive_gear_ratio);
    steer_cmd.svel = 400 ; //   400°/s

    // steer_cmd.scmd = steerWheelRateLimiter( desired_steer_wheel, 
    //                                                        prev_steer_cmd_.scmd, 
    //                                                        current_time, 
    //                                                        prev_steer_cmd_.header.stamp,
    //                                                        deg2rag(steer_cmd.svel) /*steer_cmd.svel * (3.1415926/180)*/ , 
    //                                                        current_steer_wheel, 
    //                                                        engage_cmd_);

  steer_cmd.scmd = desired_steer_wheel;
  RCLCPP_ERROR(get_logger(), "desired_steer_wheel = %f, steer_cmd.scmd = %f, current_steer_wheel = %f",
                desired_steer_wheel, steer_cmd.scmd, current_steer_wheel);

    steer_cmd.scmd = rad2deg(steer_cmd.scmd);
    steering_command_pub_-> publish(steer_cmd);
    prev_steer_cmd_ = steer_cmd;
  }


  // /* publish raw steering cmd for debug */
  // {
  //   pacmod3_msgs::msg::SteeringCmd raw_steer_cmd;
  //   raw_steer_cmd.header.frame_id = base_frame_id_;
  //   raw_steer_cmd.header.stamp = current_time;
  //   raw_steer_cmd.enable = engage_cmd_;
  //   raw_steer_cmd.ignore_overrides = false;
  //   raw_steer_cmd.clear_override = clear_override;
  //   raw_steer_cmd.command = desired_steer_wheel;
  //   raw_steer_cmd.rotation_rate =
  //     control_cmd_ptr_->lateral.steering_tire_rotation_rate * adaptive_gear_ratio;
  //   raw_steer_cmd_pub_->publish(raw_steer_cmd);
  // }



#if 0
  if (turn_indicators_cmd_ptr_ && hazard_lights_cmd_ptr_) {
    /* publish shift cmd */
    pacmod3_msgs::msg::SystemCmdInt turn_cmd;
    turn_cmd.header.frame_id = base_frame_id_;
    turn_cmd.header.stamp = current_time;
    turn_cmd.enable = engage_cmd_;
    turn_cmd.ignore_overrides = false;
    turn_cmd.clear_override = clear_override;
    turn_cmd.command =
      toPacmodTurnCmdWithHazardRecover(*turn_indicators_cmd_ptr_, *hazard_lights_cmd_ptr_);
    turn_cmd_pub_->publish(turn_cmd);
  }
  #endif

    /* publish shift cmd */
  if (turn_indicators_cmd_ptr_ && hazard_lights_cmd_ptr_) {

    remote_by_wire_driver_msgs::msg::TurnSignalCommand turn_cmd;
    turn_cmd.header.frame_id = base_frame_id_;
    turn_cmd.header.stamp = current_time;
    turn_cmd.trncmd = remote_by_wire_driver_msgs::msg::TurnSignalCommand::OFF;  // 关闭转向
    // turn_cmd.trncmd = toSkodaTurnCmdWithHazardRecover(*turn_signal_cmd_ptr_);
    turn_cmd.high_beam = false;
    turn_cmd.wiper_slow = false;
    turn_cmd.wiper_fast = false;
    turn_cmd.horn = false;
    turn_cmd.sys_wire_control_request = false;
    turn_signal_command_pub_->publish(turn_cmd);
  }

}

double SkodaInterface::calcSteerWheelRateCmd(const double gear_ratio)
{
  
  #if 0
  const auto current_vel =
    std::fabs(calculateVehicleVelocity(*wheel_speed_rpt_ptr_, *gear_cmd_rpt_ptr_));

  // send low steer rate at low speed
  if (current_vel < std::numeric_limits<double>::epsilon()) {
    return steering_wheel_rate_stopped_;
  } else if (current_vel < low_vel_thresh_) {
    return steering_wheel_rate_low_vel_;
  }

  if (!enable_steering_rate_control_) {
    return max_steering_wheel_rate_;
  }

  constexpr double margin = 1.5;
  const double rate = margin * control_cmd_ptr_->lateral.steering_tire_rotation_rate * gear_ratio;
  return std::min(std::max(std::fabs(rate), min_steering_wheel_rate_), max_steering_wheel_rate_);
  #else
  return gear_ratio;
  #endif
}

double SkodaInterface::calculateVehicleVelocity(
  const pacmod3_msgs::msg::WheelSpeedRpt & wheel_speed_rpt,
  const pacmod3_msgs::msg::SystemRptInt & shift_rpt)
{
  const double sign = (shift_rpt.output == pacmod3_msgs::msg::SystemRptInt::SHIFT_REVERSE) ? -1 : 1;
  const double vel =
    (wheel_speed_rpt.rear_left_wheel_speed + wheel_speed_rpt.rear_right_wheel_speed) * 0.5 *
    tire_radius_ * speed_scale_factor_;
  return sign * vel;
}

double SkodaInterface::calculateVariableGearRatio(const double vel, const double steer_wheel)
{
  return std::max(
    1e-5, vgr_coef_a_ + vgr_coef_b_ * vel * vel - vgr_coef_c_ * std::fabs(steer_wheel));
}

// uint16_t SkodaInterface::toPacmodShiftCmd(
//   const autoware_auto_vehicle_msgs::msg::GearCommand & gear_cmd)
// {
//   using pacmod3_msgs::msg::SystemCmdInt;

//   if (gear_cmd.command == autoware_auto_vehicle_msgs::msg::GearCommand::PARK) {
//     return SystemCmdInt::SHIFT_PARK;
//   }
//   if (gear_cmd.command == autoware_auto_vehicle_msgs::msg::GearCommand::REVERSE) {
//     return SystemCmdInt::SHIFT_REVERSE;
//   }
//   if (gear_cmd.command == autoware_auto_vehicle_msgs::msg::GearCommand::DRIVE) {
//     return SystemCmdInt::SHIFT_FORWARD;
//   }
//   if (gear_cmd.command == autoware_auto_vehicle_msgs::msg::GearCommand::LOW) {
//     return SystemCmdInt::SHIFT_LOW;
//   }

//   return SystemCmdInt::SHIFT_NONE;
// }

uint16_t SkodaInterface::toSkodaShiftCmd( const autoware_auto_vehicle_msgs::msg::GearCommand & gear_cmd)
{
  using remote_by_wire_driver_msgs::msg::GearCommand ;
  if (gear_cmd.command == autoware_auto_vehicle_msgs::msg::GearCommand::PARK) {
    return remote_by_wire_driver_msgs::msg::GearCommand::PARK;
  }
  if (gear_cmd.command == autoware_auto_vehicle_msgs::msg::GearCommand::REVERSE) {
    return remote_by_wire_driver_msgs::msg::GearCommand::REVERSE;
  }
  if (gear_cmd.command == autoware_auto_vehicle_msgs::msg::GearCommand::DRIVE) {
    return remote_by_wire_driver_msgs::msg::GearCommand::DRIVE;
  }
  if (gear_cmd.command == autoware_auto_vehicle_msgs::msg::GearCommand::LOW) {
    return remote_by_wire_driver_msgs::msg::GearCommand::DRIVE;
  }
  return remote_by_wire_driver_msgs::msg::GearCommand::NEUTRAL;
}


// std::optional<int32_t> SkodaInterface::toAutowareShiftReport(
//   const pacmod3_msgs::msg::SystemRptInt & shift)
// {
//   using autoware_auto_vehicle_msgs::msg::GearReport;
//   using pacmod3_msgs::msg::SystemRptInt;

//   if (shift.output == SystemRptInt::SHIFT_PARK) {
//     return GearReport::PARK;
//   }
//   if (shift.output == SystemRptInt::SHIFT_REVERSE) {
//     return GearReport::REVERSE;
//   }
//   if (shift.output == SystemRptInt::SHIFT_FORWARD) {
//     return GearReport::DRIVE;
//   }
//   if (shift.output == SystemRptInt::SHIFT_LOW) {
//     return GearReport::LOW;
//   }
//   return {};
// }

std::optional<int32_t>  SkodaInterface::toAutowareShiftReport(const remote_by_wire_driver_msgs::msg::GearReport & gear_report)
{
  using autoware_auto_vehicle_msgs::msg::GearReport;
  using remote_by_wire_driver_msgs::msg::GearCommand;

  if (gear_report.cmd == GearCommand::PARK) {
    return GearReport::PARK;
  }
  if (gear_report.cmd == GearCommand::REVERSE) {
    return GearReport::REVERSE;
  }
  if (gear_report.cmd == GearCommand::DRIVE) {
    return GearReport::DRIVE;
  }
  if (gear_report.cmd == GearCommand::SPORT) {
    return GearReport::LOW;
  }
  return {};
}


uint16_t SkodaInterface::toPacmodTurnCmd(
  const autoware_auto_vehicle_msgs::msg::TurnIndicatorsCommand & turn,
  const autoware_auto_vehicle_msgs::msg::HazardLightsCommand & hazard)
{
  using autoware_auto_vehicle_msgs::msg::HazardLightsCommand;
  using autoware_auto_vehicle_msgs::msg::TurnIndicatorsCommand;
  using pacmod3_msgs::msg::SystemCmdInt;

  // NOTE: hazard lights command has a highest priority here.
  if (hazard.command == HazardLightsCommand::ENABLE) {
    return SystemCmdInt::TURN_HAZARDS;
  }
  if (turn.command == TurnIndicatorsCommand::ENABLE_LEFT) {
    return SystemCmdInt::TURN_LEFT;
  }
  if (turn.command == TurnIndicatorsCommand::ENABLE_RIGHT) {
    return SystemCmdInt::TURN_RIGHT;
  }
  return SystemCmdInt::TURN_NONE;
}

uint16_t SkodaInterface::toPacmodTurnCmdWithHazardRecover(
  const autoware_auto_vehicle_msgs::msg::TurnIndicatorsCommand & turn,
  const autoware_auto_vehicle_msgs::msg::HazardLightsCommand & hazard)
{
  #if 0
  using pacmod3_msgs::msg::SystemRptInt;

  if (!engage_cmd_ || turn_rpt_ptr_->command == turn_rpt_ptr_->output) {
    last_shift_inout_matched_time_ = this->now();
    return toPacmodTurnCmd(turn, hazard);
  }

  if ((this->now() - last_shift_inout_matched_time_).seconds() < hazard_thresh_time_) {
    return toPacmodTurnCmd(turn, hazard);
  }

  // hazard recover mode
  if (hazard_recover_count_ > hazard_recover_cmd_num_) {
    last_shift_inout_matched_time_ = this->now();
    hazard_recover_count_ = 0;
  }
  hazard_recover_count_++;

  if (
    turn_rpt_ptr_->command != SystemRptInt::TURN_HAZARDS &&
    turn_rpt_ptr_->output == SystemRptInt::TURN_HAZARDS) {
    // publish hazard commands for turning off the hazard lights
    return SystemRptInt::TURN_HAZARDS;
  } else if (  // NOLINT
    turn_rpt_ptr_->command == SystemRptInt::TURN_HAZARDS &&
    turn_rpt_ptr_->output != SystemRptInt::TURN_HAZARDS) {
    // publish none commands for turning on the hazard lights
    return SystemRptInt::TURN_NONE;
  } else {
    // something wrong
    RCLCPP_ERROR_STREAM(
      get_logger(), "turn signal command and output do not match. "
                      << "COMMAND: " << turn_rpt_ptr_->command
                      << "; OUTPUT: " << turn_rpt_ptr_->output);
    return toPacmodTurnCmd(turn, hazard);
  }
  #else
  return 0;
  #endif
}

int32_t SkodaInterface::toAutowareTurnIndicatorsReport(
  const pacmod3_msgs::msg::SystemRptInt & turn)
{
  using autoware_auto_vehicle_msgs::msg::TurnIndicatorsReport;
  using pacmod3_msgs::msg::SystemRptInt;

  if (turn.output == SystemRptInt::TURN_RIGHT) {
    return TurnIndicatorsReport::ENABLE_RIGHT;
  } else if (turn.output == SystemRptInt::TURN_LEFT) {
    return TurnIndicatorsReport::ENABLE_LEFT;
  } else if (turn.output == SystemRptInt::TURN_NONE) {
    return TurnIndicatorsReport::DISABLE;
  }
  return TurnIndicatorsReport::DISABLE;
}

int32_t SkodaInterface::toAutowareHazardLightsReport(
  const pacmod3_msgs::msg::SystemRptInt & hazard)
{
  using autoware_auto_vehicle_msgs::msg::HazardLightsReport;
  using pacmod3_msgs::msg::SystemRptInt;

  if (hazard.output == SystemRptInt::TURN_HAZARDS) {
    return HazardLightsReport::ENABLE;
  }

  return HazardLightsReport::DISABLE;
}

double SkodaInterface::steerWheelRateLimiter(
  const double current_steer_cmd, const double prev_steer_cmd,
  const rclcpp::Time & current_steer_time, const rclcpp::Time & prev_steer_time,
  const double steer_rate, const double current_steer_output, const bool engage)
{
  if (!engage) {
    // return current steer as steer command ( do not apply steer rate filter )
    return current_steer_output;
  }

  const double dsteer = current_steer_cmd - prev_steer_cmd;
  const double dt = std::max(0.0, (current_steer_time - prev_steer_time).seconds());
  const double max_dsteer = std::fabs(steer_rate) * dt;
  const double limited_steer_cmd =
    prev_steer_cmd + std::min(std::max(-max_dsteer, dsteer), max_dsteer);
  return limited_steer_cmd;
}

pacmod3_msgs::msg::SystemCmdInt SkodaInterface::createClearOverrideDoorCommand()
{
  pacmod3_msgs::msg::SystemCmdInt door_cmd;
  door_cmd.header.frame_id = "base_link";
  door_cmd.header.stamp = this->now();
  door_cmd.clear_override = true;
  return door_cmd;
}

pacmod3_msgs::msg::SystemCmdInt SkodaInterface::createDoorCommand(const bool open)
{
  pacmod3_msgs::msg::SystemCmdInt door_cmd;
  door_cmd.header.frame_id = "base_link";
  door_cmd.header.stamp = this->now();
  door_cmd.enable = true;

  if (open) {
    door_cmd.command = pacmod3_msgs::msg::SystemCmdInt::DOOR_OPEN;
  } else {
    door_cmd.command = pacmod3_msgs::msg::SystemCmdInt::DOOR_CLOSE;
  }
  return door_cmd;
}

void SkodaInterface::setDoor(
  const tier4_external_api_msgs::srv::SetDoor::Request::SharedPtr request,
  const tier4_external_api_msgs::srv::SetDoor::Response::SharedPtr response)
{
  if (!engage_cmd_) {
    // when the vehicle mode is manual, ignore the request.
    response->status.code = tier4_external_api_msgs::msg::ResponseStatus::IGNORED;
    response->status.message = "Current vehicle mode is manual. The request is ignored.";
    return;
  }

  #if 0
  // open/close the door
  door_cmd_pub_->publish(createClearOverrideDoorCommand());
  rclcpp::Rate(10.0).sleep();  // avoid message loss
  door_cmd_pub_->publish(createDoorCommand(request->open));
  response->status.code = tier4_external_api_msgs::msg::ResponseStatus::SUCCESS;

  if (request->open) {
    // open the door
    response->status.message = "Success to open the door.";
  } else {
    // close the door
    response->status.message = "Success to close the door.";
  }
  #endif
}

tier4_api_msgs::msg::DoorStatus SkodaInterface::toAutowareDoorStatusMsg(
  const pacmod3_msgs::msg::SystemRptInt & msg_ptr)
{
  using pacmod3_msgs::msg::SystemRptInt;
  using tier4_api_msgs::msg::DoorStatus;
  DoorStatus door_status;

  door_status.status = DoorStatus::UNKNOWN;

  if (msg_ptr.command == SystemRptInt::DOOR_CLOSE && msg_ptr.output == SystemRptInt::DOOR_OPEN) {
    // do not used (command & output are always the same value)
    door_status.status = DoorStatus::DOOR_CLOSING;
  } else if (  // NOLINT
    msg_ptr.command == SystemRptInt::DOOR_OPEN && msg_ptr.output == SystemRptInt::DOOR_CLOSE) {
    // do not used (command & output are always the same value)
    door_status.status = DoorStatus::DOOR_OPENING;
  } else if (msg_ptr.output == SystemRptInt::DOOR_CLOSE) {
    door_status.status = DoorStatus::DOOR_CLOSED;
  } else if (msg_ptr.output == SystemRptInt::DOOR_OPEN) {
    door_status.status = DoorStatus::DOOR_OPENED;
  }

  return door_status;
}
